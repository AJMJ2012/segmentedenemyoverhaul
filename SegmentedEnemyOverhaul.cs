using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;
using Terraria.ModLoader;
using Terraria;

namespace SegmentedEnemyOverhaul {
	public class SegmentedEnemyOverhaul : Mod {
		public SegmentedEnemyOverhaul() {
			Properties = new ModProperties() {
				Autoload = true
			};
		}
	}

	public class GNPC : GlobalNPC {

		public override void SetDefaults(NPC npc) {
			if (npc.realLife >= 0 && npc.whoAmI != npc.realLife) {
				npc.npcSlots = 0; // Makes segments not adhere to maximum NPC limits
				NPC parent = Main.npc[npc.realLife]; // Just in case some mod sets different stats on segments, when really they should all be the same.
				npc.damage = parent.damage;
				npc.defDamage = parent.defDamage;
				npc.defDefense = parent.defDefense;
				npc.defense = parent.defense;
				npc.life = parent.life;
				npc.lifeMax = parent.lifeMax;
			}
		}

		public override void ModifyHitByItem (NPC npc, Player player, Item item, ref int damage, ref float knockback, ref bool crit) {
			ModifyCritArea(npc, ref crit);
		}

		public override void ModifyHitByProjectile (NPC npc, Projectile projectile, ref int damage, ref float knockback, ref bool crit, ref int hitDirection) {
			ModifyCritArea(npc, ref crit);
		}

		private void ModifyCritArea(NPC npc, ref bool crit) {
			if (npc.realLife >= 0) { // As long as it respects using ai[0] for segments
				if (npc.whoAmI == npc.realLife) { // Shooting the head always gives a critical hit
					crit = true;
				}
				if (npc.ai[0] == 0) { // Shooting the tail tip never gives a critical hit
					crit = false;
				}
			}
		}

		public override void UpdateLifeRegen (NPC npc, ref int damage) {
			if (npc.realLife >= 0 && npc.whoAmI != npc.realLife) { // Stops segments other than the head from taking regen damage or giving regen life. Prevents cheesing damage with debuffs like fire.
				damage = 0;
				npc.lifeRegen = 0;
			}
		}

		public override void OnHitByProjectile (NPC npc, Projectile projectile, int damage, float knockback, bool crit) {
			MakeSegmentsImmune(npc, projectile.owner);
		}

		public override void OnHitByItem (NPC npc, Player player, Item item, int damage, float knockback, bool crit) {
			MakeSegmentsImmune(npc, player.whoAmI);
		}

		private void MakeSegmentsImmune(NPC npc, int id) { // Makes all segments of a segmented NPC immune when hit to avoid insane damages (better than multiplying health by number of segments)
			if (npc.realLife >= 0) { // As long as it respects using ai[0] for segments
				bool last = false;
				NPC parent = Main.npc[npc.realLife];
				parent.lifeRegen = npc.lifeRegen; // Make the head use the life regen that segment uses
				int i = 0;
				while (parent.ai[0] > 0 || last) {
					parent.immune[id] = npc.immune[id];
					for (int j=0; j < npc.buffType.Length; j++) { // Share the debuffs between all segments
						if (npc.buffType[j] > 0 && npc.buffTime[j] > 0) {
							parent.buffType[j] = npc.buffType[j];
							parent.buffTime[j] = npc.buffTime[j];
						}
					}
					if (last) { break; }
					parent = Main.npc[(int)parent.ai[0]];
					if (parent.ai[0] == 0) { last = true; } // If it's the tail tip
					if (i++ > 200) { throw new System.InvalidOperationException("Recursion detected"); break; } // Just in case
				}
			}
		}
	}
}